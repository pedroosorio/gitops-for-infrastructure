PRIVATE=Private.mak
ROOT=$(shell pwd)
TARGET ?=

source:
	@touch $(ROOT)/$(PRIVATE)
	@export $(cat $(ROOT)/$(PRIVATE) | xargs) > /dev/null

# Pull the latest version of these files to ephemeral /environment directory
refresh-environment:
	@terraform -chdir=terraform taint local_sensitive_file.private_key
	@terraform -chdir=terraform taint local_sensitive_file.inventory
	@terraform -chdir=terraform taint local_sensitive_file.ssh_config
	@terraform -chdir=terraform apply -auto-approve \
		-target local_sensitive_file.private_key \
		-target local_sensitive_file.inventory \
		-target local_sensitive_file.ssh_config

init: source
	@cat terraform/templates/state.tpl | envsubst > terraform/state.tf
	@terraform -chdir=terraform init

clean:
	@rm -rf terraform/state.tf terraform/.terraform

taint:
	terraform -chdir=terraform taint '$(TARGET)'

plan:
	@terraform -chdir=terraform plan

apply:
	@terraform -chdir=terraform apply

auto-apply:
	@terraform -chdir=terraform apply -auto-approve

destroy:
	@terraform -chdir=terraform destroy

format:
	@terraform fmt -recursive .

validate:
	@terraform -chdir=terraform validate
	@ansible-lint ansible/

ping:
	@ansible all -m ping
	@ansible all -a "sh -c 'echo \"----- OS -----\" ; cat /etc/os-release ; printf \"\n----- User Data -----\n\" ; cat /etc/user-data.json | jq .'"
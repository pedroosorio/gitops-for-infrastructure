#!/bin/sh
set -e
echo "Mounting attached volumes ..."

USER_DATA_FILE="/etc/user-data.json"
VOLUMES=$(jq -r ".volumes // []" $USER_DATA_FILE)

for VOLUME in $(echo "$VOLUMES" | jq -r '.[] | @base64'); do
    VOLUME_DATA=$(echo $VOLUME | base64 --decode | jq -r .)
    VOLUME_ID=$(echo $VOLUME_DATA | jq -r .volume)
    VOLUME_MOUNT_PATH=$(echo $VOLUME_DATA | jq -r .mount_path)
    VOLUME_DEVICE=$(echo $VOLUME_DATA | jq -r .device)

    echo "Mounting volume '$VOLUME_ID'"

    # Check if device exists
    ls $VOLUME_DEVICE > /dev/null 2>&1
    if [ $? -ne 0 ]; then
        echo "Device '$VOLUME_DEVICE' not found"
        exit 1
    fi

    mkdir -p $VOLUME_MOUNT_PATH
    mount $VOLUME_DEVICE $VOLUME_MOUNT_PATH
    echo "Volume '$VOLUME_DEVICE' mounted to '$VOLUME_MOUNT_PATH'"
done
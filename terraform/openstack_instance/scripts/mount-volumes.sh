echo "Mounting attached volumes ..."

USER_DATA_FILE="/etc/user-data.json"
VOLUMES=$(jq -r ".volumes // []" $USER_DATA_FILE)

for VOLUME in $(echo "$VOLUMES" | jq -r '.[] | @base64'); do
    VOLUME_DATA=$(echo $VOLUME | base64 --decode | jq -r .)
    VOLUME_ID=$(echo $VOLUME_DATA | jq -r .volume)
    VOLUME_MOUNT_PATH=$(echo $VOLUME_DATA | jq -r .mount_path)
    VOLUME_DEVICE=$(echo $VOLUME_DATA | jq -r .device)
    INIT_VOLUME_FROM_MOUNT_PATH=$(echo $VOLUME_DATA | jq -r '.init_from_mount_path // false')
    UPDATE_VOLUME_FROM_MOUNT_PATH=$(echo $VOLUME_DATA | jq -r '.update_from_mount_path // false')

    echo "Mounting volume '$VOLUME_ID'"

    # Check if device exists
    ls $VOLUME_DEVICE > /dev/null 2>&1
    if [ $? -ne 0 ]; then
        echo "Device '$VOLUME_DEVICE' not found"
        exit 1
    fi

    # Check if device has a valid ext4 filesystem
    if [ $(file -sL $VOLUME_DEVICE | grep ": data" | wc -l) -eq 1 ]; then
        echo "Volume is blank. Creating ext4 filesystem"
        mkfs.ext4 $VOLUME_DEVICE
    else
        echo "Volume already has a filesystem"
    fi

    mkdir -p $VOLUME_MOUNT_PATH
    if [ "$INIT_VOLUME_FROM_MOUNT_PATH" = "true" ]; then
        echo "Initializing volume from '$VOLUME_MOUNT_PATH'"
        TEMPORARY_MOUNT="/mnt/$(date +%s | md5sum | sed 's/ .*//' | head -c 8)"
        mkdir -p $TEMPORARY_MOUNT
        mount $VOLUME_DEVICE $TEMPORARY_MOUNT

        if [ "$UPDATE_VOLUME_FROM_MOUNT_PATH" = "true" ] || [ $(ls -al $TEMPORARY_MOUNT | grep -Ev '\.|\.\.|lost\+found|total [0-9]+' | wc -l) -eq 0 ]; then
            cp -a $VOLUME_MOUNT_PATH/. $TEMPORARY_MOUNT/
        else
            echo "Volume already contains data, skipping initialization from '$VOLUME_MOUNT_PATH'"
        fi
        
        umount $VOLUME_DEVICE
        rm -rf $TEMPORARY_MOUNT
    fi

    mount $VOLUME_DEVICE $VOLUME_MOUNT_PATH
    echo "Volume '$VOLUME_DEVICE' mounted to '$VOLUME_MOUNT_PATH'"
done

mkdir -p /etc/startup/logs
echo "${startup_mount_volumes_script}" | base64 --decode > /etc/startup/mount-volumes.sh
chmod +x /etc/startup/mount-volumes.sh
crontab -u root -l > /etc/startup/crontab || touch /etc/startup/crontab
echo "@reboot sh /etc/startup/mount-volumes.sh > /etc/startup/logs/mount-volumes.log 2>&1" >> /etc/startup/crontab
crontab -u root /etc/startup/crontab
output "instance" {
  value = {
    id     = openstack_compute_instance_v2.instance.id
    name   = openstack_compute_instance_v2.instance.name
    flavor = openstack_compute_instance_v2.instance.flavor_name
    image = {
      name = data.openstack_images_image_v2.image.name
      id   = data.openstack_images_image_v2.image.id
    }
    user = local.config.user
    ssh = {
      user                = local.config.user
      key_pair            = local.config.ssh.keypair
      host                = local.floating_ip != null ? local.floating_ip.address : openstack_compute_instance_v2.instance.access_ip_v4
      jump_host           = local.floating_ip == null ? openstack_compute_instance_v2.instance.access_ip_v4 : null
      users               = concat([local.config.user], [])
      allowed_cidr_blocks = [for rule in openstack_networking_secgroup_rule_v2.ssh_sg_rule : rule.remote_ip_prefix]
    }
    config = local.config
    network = {
      interfaces = openstack_compute_instance_v2.instance.network
      ip = {
        v4 = openstack_compute_instance_v2.instance.access_ip_v4
        v6 = openstack_compute_instance_v2.instance.access_ip_v6
      }
      floating_ip = {
        id : local.floating_ip != null ? local.floating_ip.id : "N/A"
        address : local.floating_ip != null ? local.floating_ip.address : "N/A"
      }
      security_groups = local.instance_security_groups
    }
    volumes = local.volumes
  }
}
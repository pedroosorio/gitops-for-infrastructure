locals {
  floating_ip = local.config.floating_ip.associate ? (
    local.config.floating_ip.create ? openstack_networking_floatingip_v2.floating_ip[0] : data.openstack_networking_floatingip_v2.floating_ip[0]
  ) : null
}

data "openstack_networking_network_v2" "network" {
  name = local.config.network
}

data "openstack_networking_subnet_v2" "network_subnets" {
  for_each  = toset(data.openstack_networking_network_v2.network.subnets)
  subnet_id = each.value
}

data "openstack_networking_floatingip_v2" "floating_ip" {
  count   = !local.config.floating_ip.create && local.config.floating_ip.associate ? 1 : 0
  address = local.config.floating_ip.address
}

data "openstack_networking_network_v2" "floating_ip_network" {
  count = local.config.floating_ip.create && local.config.floating_ip.associate ? 1 : 0
  name  = local.config.floating_ip.network
}

resource "openstack_networking_floatingip_v2" "floating_ip" {
  count      = local.config.floating_ip.create && local.config.floating_ip.associate ? 1 : 0
  address    = local.config.floating_ip.address
  pool       = local.config.floating_ip.network
  subnet_ids = data.openstack_networking_network_v2.floating_ip_network[0].subnets
}

resource "openstack_compute_floatingip_associate_v2" "floating_ip" {
  count                 = local.config.floating_ip.associate ? 1 : 0
  floating_ip           = local.floating_ip.address
  instance_id           = openstack_compute_instance_v2.instance.id
  fixed_ip              = openstack_compute_instance_v2.instance.access_ip_v4
  wait_until_associated = true
}
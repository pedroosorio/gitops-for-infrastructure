# "Module" specific variables
variable "openstack_image_name_regex" {
  type    = string
  default = "^.*(u|U)buntu((-[a-zA-Z]+)+|)-([0-9]{1,3}\\.[0-9]{1,3})(-nogui|$)"
}

# Get the default image to use when creating instances
data "openstack_images_image_ids_v2" "images" {
  name_regex    = var.openstack_image_name_regex
  member_status = "accepted"
  sort          = "name:desc,status"
}

# Use an external data source to get the name of the image given its id, as the openstack provider does not support the operation
data "external" "image_info" {
  program = [
    "sh",
    "-c",
    "openstack --os-cloud ${var.openstack.cloud} image show ${data.openstack_images_image_ids_v2.images.ids[0]} -f json | jq -r '. | {name: .name}'"
  ]
}

data "openstack_images_image_v2" "default_image" {
  name        = data.external.image_info.result["name"]
  most_recent = true
}

# Validate openstack resource configurations
# TODO: Add a external data source to check for flavor
data "openstack_images_image_v2" "image" {
  name        = local.config.image
  most_recent = true
}
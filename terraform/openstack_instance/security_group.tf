locals {
  subnet_cidr_blocks = tolist([for subnet in data.openstack_networking_subnet_v2.network_subnets : {
    cidr        = subnet.cidr
    description = "Subnet ${subnet.name} access"
  }])

  user_cidr_blocks = tolist(flatten([for user in keys(var.users) :
    [
      for cidr in coalesce(lookup(var.users, user).cidrs, []) : {
        cidr        = cidr
        description = "User '${user}' access"
      }
    ]
  ]))

  extra_allowed_networks_names   = toset([for extra in local.config.ssh.extra_access : extra.network if extra.network != null])
  extra_allowed_networks_subnets = toset(flatten([for network in data.openstack_networking_network_v2.extra_network : network.subnets]))
  extra_allowed_networks_cidrs = tolist([for subnet in data.openstack_networking_subnet_v2.extra_network_subnets : {
    cidr        = subnet.cidr
    description = "Subnet ${subnet.name} access"
  }])

  instance_is_externally_reachable = local.config.floating_ip != null
  ssh_access_cidr_blocks = setunion(
    local.config.ssh.enable_public_access ? [{ cidr = "0.0.0.0/0", description = "Public access" }] : [],
    local.config.ssh.enable_internal_access && !local.config.ssh.enable_public_access ? local.subnet_cidr_blocks : [],
    local.instance_is_externally_reachable && !local.config.ssh.enable_public_access ? var.trusted_cidr_blocks : [],
    local.instance_is_externally_reachable && !local.config.ssh.enable_public_access ? local.user_cidr_blocks : [],
    !local.config.ssh.enable_public_access ? local.extra_allowed_networks_cidrs : []
  )

  instance_security_groups = concat(local.config.security_groups, [openstack_networking_secgroup_v2.ssh_security_group.name])
}

data "openstack_networking_network_v2" "extra_network" {
  for_each = local.extra_allowed_networks_names
  name     = each.value
}

data "openstack_networking_subnet_v2" "extra_network_subnets" {
  for_each  = toset(local.extra_allowed_networks_subnets)
  subnet_id = each.value
}

resource "openstack_networking_secgroup_v2" "ssh_security_group" {
  name        = "${local.instance_name}-ssh-sg"
  description = "${local.instance_name} SSH security group"
}

resource "openstack_networking_secgroup_rule_v2" "ssh_sg_rule" {
  for_each = { for cidr_block in local.ssh_access_cidr_blocks : cidr_block.cidr => cidr_block.description... }

  security_group_id = openstack_networking_secgroup_v2.ssh_security_group.id
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = each.key
  description       = try(join("\n", each.value), each.value)
}
# Configurations with default values
locals {
  config = defaults(var.config, {
    name  = tostring(random_id.name.id)
    image = tostring(data.openstack_images_image_v2.default_image.name)
    user  = "ubuntu"
    ssh = {
      enable_public_access   = false
      enable_internal_access = false
    }
    floating_ip = {
      create    = false
      associate = false
      network   = "__undefined__"
    }
  })

  instance_name = "${var.environment.resource_prefix}-${local.config.name}"
  volumes = [for volume in keys(local.config.volumes) :
    merge({ volume = volume }, lookup(local.config.volumes, volume))
  ]

  cloud_init = [
    templatefile("${path.module}/scripts/install-core-dependencies.sh", {}),
    length(local.volumes) > 0 ? templatefile("${path.module}/scripts/mount-volumes.sh", {
      startup_mount_volumes_script = base64encode(file("${path.module}/scripts/startup/mount-volumes.sh"))
    }) : ""
  ]
  user_data_info = {
    volumes = [for volume in openstack_blockstorage_volume_v3.volume :
      {
        volume                 = volume.id
        device                 = volume.metadata["device"]
        mount_path             = volume.metadata["mount_path"]
        init_from_mount_path   = tobool(volume.metadata["init_from_mount_path"])
        update_from_mount_path = tobool(volume.metadata["update_from_mount_path"])
      }
    ]
  }
}

# Generate random instance name
resource "random_id" "name" {
  byte_length = 8
}

resource "openstack_blockstorage_volume_v3" "volume" {
  count             = length(local.volumes)
  availability_zone = local.config.az
  name              = "${local.instance_name}-${local.volumes[count.index].volume}-volume"
  description       = "${local.instance_name} ${local.volumes[count.index].volume} volume"
  size              = local.volumes[count.index].size
  metadata = merge({
    environment            = var.environment.name
    volume_id              = local.volumes[count.index].volume
    boot_index             = count.index + 1
    device                 = "/dev/vd${substr(local.alphabet, count.index + 1, 1)}"
    mount_path             = lookup(local.volumes[count.index], "mount_path", "")
    persist                = local.volumes[count.index].persist == true
    init_from_mount_path   = local.volumes[count.index].init_from_mount_path == true
    update_from_mount_path = local.volumes[count.index].update_from_mount_path == true
  }, lookup(local.volumes[count.index], "metadata", {}))
}

resource "openstack_compute_instance_v2" "instance" {
  availability_zone = local.config.az
  name              = local.instance_name
  flavor_name       = local.config.flavor
  key_pair          = local.config.ssh.keypair
  security_groups   = local.instance_security_groups
  image_id          = data.openstack_images_image_v2.image.id
  metadata = {
    environment = var.environment.name
    name        = local.config.name
    user        = local.config.user
    key_pair    = local.config.ssh.keypair
  }

  user_data = <<EOT
#!/bin/sh
set -e
mkdir -p /etc/startup
echo "${base64encode(jsonencode(local.user_data_info))}" | base64 --decode > /etc/user-data.json
echo "Initializing instance ..."
${join("\n", local.cloud_init)}
echo "Instance finished initialization"
touch /etc/startup/initialized
EOT

  network {
    uuid = data.openstack_networking_network_v2.network.id
  }

  block_device {
    uuid                  = data.openstack_images_image_v2.image.id
    source_type           = "image"
    destination_type      = "local"
    boot_index            = 0
    delete_on_termination = true
  }

  dynamic "block_device" {
    for_each = { for volume in openstack_blockstorage_volume_v3.volume : volume.id => volume }
    content {
      uuid                  = block_device.key
      source_type           = "volume"
      destination_type      = "volume"
      boot_index            = block_device.value.metadata.boot_index
      delete_on_termination = !block_device.value.metadata.persist
      guest_format          = "ext4"
    }
  }
}
locals {
  alphabet = "abcdefghijklmnopqrstuvwxz"
}

variable "environment" {
  type = object({
    name            = string
    resource_prefix = string
  })
}

variable "openstack" {
  type = object({
    cloud      = string
    project_id = string
    az         = string
  })
}

variable "trusted_cidr_blocks" {
  type = list(object({
    cidr        = string
    description = string
  }))
}

variable "users" {
  type = map(object({
    cidrs    = optional(list(string))
    ssh_keys = list(string)
    admin    = optional(bool)
  }))
}

variable "config" {
  type = object({
    name            = optional(string)
    image           = optional(string)
    user            = optional(string)
    az              = string
    flavor          = string
    network         = string
    security_groups = optional(list(string))
    floating_ip = object({
      associate = optional(bool)
      create    = optional(bool)
      network   = optional(string)
      address   = optional(string)
    })
    ssh = object({
      keypair                = string
      enable_public_access   = optional(bool)
      enable_internal_access = optional(bool)
      extra_access = optional(list(object({
        cidr        = optional(string)
        network     = optional(string)
        description = optional(string)
      })))
    })
    volumes = optional(map(object({
      size                   = number
      persist                = optional(bool)
      metadata               = optional(map(string))
      mount_path             = optional(string)
      init_from_mount_path   = optional(bool)
      update_from_mount_path = optional(bool)
    })))
  })
}
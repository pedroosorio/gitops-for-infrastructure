# Top level configurations
environment = {
  name            = "inov-test-env"
  resource_prefix = "inov"
}

# Openstack configurations
openstack = {
  cloud      = "engage"
  project_id = "0505002d0063496bb0dea54c2a89f356"
  az         = "nova"
}

# Networking configuration
trusted_cidr_blocks = [
  {
    cidr        = "10.8.2.0/26",
    description = "Engage VPN"
  }
]

# Users configuration
users = {
  posorio = {
    admin = true
    cidrs = [
      "161.230.92.249/32"
    ]
    ssh_keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC/v1yAx8UxOgc39OM9Hhq15fSkPbJK2pxjyEG2cXqika3x6sDSnnluZV2XafrVb0H5Hhehnvyw34iZl7aJl7Pt3HWeSYER3nhC6Njt1W+LwYHkQv3hc3RlbZydU3azWImhae7Vf8DN7QbC3lVrNHx6FLAMXq4n3T9TuJZUKQg22nP2vxnZxF8TAfRSGR85O3CffEayUUxrBBOmCIKI3Vjua11Laij+4HsSupzgkOAdPxXMFcTaMGaUT7ajN3f/k9JYHI/XEAJ/kyK8NxCijS+fxMItKdx8Rl9l9O9WKVPvRkjwoMWsM+GG/eiUbRioki1TYznjuWUwbayCPYXKkEZl posorio@DESKTOP-80LLD3O"
    ]
  }
  engelec = {
    admin = true
    ssh_keys = [
      "AAAAB3NzaC1yc2EAAAADAQABAAACAQCyZZZeOBuL79pXiXCTd5ZVARHUxX8e1ex4a7hrVYAhv990mo7Td1wyk0uOtVKkECzH4f1IYvBx9WGcGrvTHoDfiszKrUaVhWU7w2Bdy9WorEwhIdbTICJMwfo6dULuBVuOXmGJoP6q2+ZtKwjlZZfBxBskOk3TAB+Lhja6ztteCQkaIsVPUClxdeQMYrISjzkcw/ccpuQpVxYGZbsfDyuD2OGL4GcbYd7rctiZXzQxQQD9VXBAz/cbKnhWvaOCzgroxnp9obsSUb0MrGUc+aURrMX/5jbL69C8oLIgH6x4xUxbAu1EOPHddOfNWDHHnudPGbn3lIbvrPtrw5yyDOPHHuTBWHD1fmFNWydUdNBjzOlbY9Bg4OM1RYaudnaKwsWc42FsrF5Xo0GPOeJzfqQZEwDw6GDzGpMu25QmsabRix0mclHhfebU/y+QG2AUPObFp/AqzCfnKY+gkRa7FJ5vFY9kExSdjM+Z6GddrP+TYavGkkEVMR0taBP8KIaQLUZN3cgBjkHOdJ910/kFFC+aigPXquCRF3dRZI0PPujwsMV9P1AFuHc24nh1XjSj22bHDEnXM4tFX64wUjAUKgvg0T7CGIc4zJI4G5triROss1jy3SToxTIH3fA3BwDoiAtMHFTk8g0/iDX/zdaD2k/AFOqCyVuAbCjeb/FFYSwKmw== bruno.engelec@gmail.com"
    ]
  }
}
terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/38960259/terraform/state/$ENVIRONMENT-terraform-state"
    lock_address   = "https://gitlab.com/api/v4/projects/38960259/terraform/state/$ENVIRONMENT-terraform-state/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/38960259/terraform/state/$ENVIRONMENT-terraform-state/lock"
    username       = "$GITLAB_USERNAME"
    password       = "$GITLAB_ACCESS_TOKEN"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }
}
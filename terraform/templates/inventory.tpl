%{ for group, instances in node_groups ~}
${group}:
  hosts:
%{ for instance in instances ~}
    ${instance.name}:
      ansible_user: ${instance.ssh.user}
      ansible_host: ${instance.name}
%{ endfor ~}
%{ endfor ~}

# Global configurations
all:
  children:
%{ for group, instances in node_groups ~}
    ${group}:
%{ endfor ~}
  vars:
    ansible_python_interpreter: python3
    ${indent(4, yamlencode(vars))}

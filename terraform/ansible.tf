# TODO: Create a node-group module

locals {
  environment_directory = "${path.module}/../environment"
  node_groups = {
    bastion = [
      module.bastion_instance.instance
    ]
  }
}

resource "local_sensitive_file" "inventory" {
  content = templatefile("${path.module}/templates/inventory.tpl", {
    node_groups = local.node_groups
    vars = {
      users = [for user in keys(var.users) :
        {
          name     = user
          ssh_keys = coalesce(lookup(var.users, user).ssh_keys, [])
          admin    = coalesce(lookup(var.users, user).admin, false)
        }
      ]
    }
  })
  filename        = "${local.environment_directory}/inventory.yml"
  file_permission = "0600"
}

resource "local_sensitive_file" "ssh_config" {
  content = templatefile("${path.module}/templates/ssh.tpl", {
    node_groups           = local.node_groups
    environment_directory = abspath(local.environment_directory)
  })
  filename        = "${local.environment_directory}/ssh.config"
  file_permission = "0600"
}
variable "environment" {
  type = object({
    name            = string
    resource_prefix = string
  })
}

variable "openstack" {
  type = object({
    cloud      = string
    project_id = string
    az         = string
  })
}

variable "instances" {
  type    = list(string)
  default = []
}

variable "playbook_target" {
  type    = string
  default = "all"
}

variable "playbooks" {
  type = list(object({
    id   = string
    vars = any
  }))
  default = []
}
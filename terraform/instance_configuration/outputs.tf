output "configuration" {
  value = {
    instances = var.instances
    config    = var.playbooks
  }
}
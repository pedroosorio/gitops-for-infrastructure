locals {
  root_directory = "${path.module}/../.."
  playbooks = {
    for playbook in var.playbooks : playbook.id => {
      vars : playbook.vars
    }
  }
}

resource "null_resource" "playbook" {
  for_each = local.playbooks
  triggers = {
    target = jsonencode(var.instances)
    vars   = jsonencode(each.value.vars)
  }

  provisioner "local-exec" {
    working_dir = abspath(local.root_directory)
    command     = "ANSIBLE_FORCE_COLOR=1 ansible-playbook ansible/playbooks/${each.key}.yml -l ${var.playbook_target}"
  }
}
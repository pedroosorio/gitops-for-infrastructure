provider "openstack" {
  cloud     = var.openstack.cloud
  tenant_id = var.openstack.project_id
}
locals {
  ssh_include_header        = "Auto-generated entry for ${var.environment.name}"
  ssh_config_path           = pathexpand("~/.ssh/config")
  update_ssh_entries_script = <<EOT
touch ${local.ssh_config_path} &&
sed '/${local.ssh_include_header}/,+3d' ${local.ssh_config_path} > ${local.ssh_config_path}.bak &&
cat <<EOF > ${local.ssh_config_path}
### ${local.ssh_include_header}
Include ${abspath(local.environment_directory)}/ssh.config
###

$(cat ${local.ssh_config_path}.bak)
EOF
EOT
}

data "local_sensitive_file" "ssh_config" {
  filename = local.ssh_config_path
}

resource "null_resource" "update_ssh_entries" {
  depends_on = [
    data.local_sensitive_file.ssh_config
  ]

  triggers = {
    script     = local.update_ssh_entries_script
    ssh_config = data.local_sensitive_file.ssh_config.content_base64
  }

  provisioner "local-exec" {
    command = self.triggers.script
  }
}
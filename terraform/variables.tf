variable "environment" {
  type = object({
    name            = string
    resource_prefix = string
  })
}

variable "openstack" {
  type = object({
    cloud      = string
    project_id = string
    az         = string
  })
}

variable "trusted_cidr_blocks" {
  type = list(object({
    cidr        = string
    description = string
  }))
  default = []
}

variable "users" {
  type = map(object({
    cidrs    = optional(list(string))
    ssh_keys = list(string)
    admin    = optional(bool)
  }))
}
locals {
  command = "cd ${abspath(path.module)}/.. ; ansible-playbook ansible/playbooks/users.yml -l bastion"
}

# Variables for this "module"
variable "bastion_config" {
  type = any
}

# TODO: Create a node-group module
module "bastion_instance" {
  source              = "./openstack_instance"
  environment         = var.environment
  openstack           = var.openstack
  trusted_cidr_blocks = var.trusted_cidr_blocks
  users               = var.users

  config = merge(
    {
      name = "bastion"
    },
    var.bastion_config,
    {
      # Hack to be able to merge nested objects/lists. These then get merged with openstack_instance module's default values
      ssh = merge({
        keypair = openstack_compute_keypair_v2.keypair.name # use the environment keypair
      }, lookup(var.bastion_config, "ssh", {}))
      floating_ip = merge({
        associate = true, # bastion needs a floating ip
        create    = true
      }, lookup(var.bastion_config, "floating_ip", {}))
      security_groups = distinct(concat(
        [openstack_networking_secgroup_v2.default_security_group.name],
        lookup(var.bastion_config, "security_groups", [])
      ))
    }
  )

  providers = {
    openstack = openstack
  }
}

module "bastion_configuration" {
  depends_on      = [module.bastion_instance, null_resource.update_ssh_entries, local_sensitive_file.ssh_config, local_sensitive_file.inventory]
  source          = "./instance_configuration"
  environment     = var.environment
  openstack       = var.openstack
  instances       = [module.bastion_instance.instance.id] # This would be the node_group instances list
  playbook_target = "bastion"                             # This would be the node_group name
  playbooks = [
    {
      id : "users",
      vars : {
        users = var.users
      }
    }
  ]
}
bastion_config = {
  az      = "nova"
  flavor  = "m1.small"
  network = "internal"
  floating_ip = {
    network = "external"
  }
  volumes = {
    home = {
      size       = 20
      persist    = true
      mount_path = "/home"
      # required to grab changes done to default user (ie, ssh key)
      init_from_mount_path   = true
      update_from_mount_path = true
    }
  }
}
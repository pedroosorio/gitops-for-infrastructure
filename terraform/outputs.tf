output "bastion_instance" {
  value = module.bastion_instance.instance
}

output "bastion_configuration" {
  value = module.bastion_configuration.configuration
}
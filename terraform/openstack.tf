locals {
  default_sg_rules = {
    egress_all_tcp_ipv4 = {
      direction = "egress"
      ethertype = "IPv4"
      protocol  = "tcp"
    }
    egress_all_tcp_ipv6 = {
      direction = "egress"
      ethertype = "IPv6"
      protocol  = "tcp"
    }
    egress_all_udp_ipv4 = {
      direction = "egress"
      ethertype = "IPv4"
      protocol  = "udp"
    }
    egress_all_udp_ipv6 = {
      direction = "egress"
      ethertype = "IPv6"
      protocol  = "udp"
    }
    ingress_all_icmp = {
      direction = "ingress"
      ethertype = "IPv4"
      protocol  = "icmp"
    }
    egress_all_icmp = {
      direction = "egress"
      ethertype = "IPv4"
      protocol  = "icmp"
    }
  }
}

# Generate a keypair to be used for SSH access to instances
resource "openstack_compute_keypair_v2" "keypair" {
  name = "${var.environment.resource_prefix}-ssh-key"
}

resource "local_sensitive_file" "private_key" {
  content         = openstack_compute_keypair_v2.keypair.private_key
  filename        = "${local.environment_directory}/${openstack_compute_keypair_v2.keypair.name}"
  file_permission = "0600"
}

# Create a default security group for the environment
resource "openstack_networking_secgroup_v2" "default_security_group" {
  name        = "${var.environment.resource_prefix}-default-sg"
  description = "${var.environment.resource_prefix} default security group"
}

resource "openstack_networking_secgroup_rule_v2" "default_rule" {
  for_each          = local.default_sg_rules
  security_group_id = openstack_networking_secgroup_v2.default_security_group.id
  direction         = each.value.direction
  ethertype         = each.value.ethertype
  protocol          = each.value.protocol
  description       = each.key

  port_range_min = lookup(each.value, "port_range_min", null)
  port_range_max = lookup(each.value, "port_range_max", null)
}